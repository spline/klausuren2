package main

import (
	"embed"
	"log"
)

// AssetFS holds static files such as HTML and CSS.
//go:embed assets/*
var AssetFS embed.FS

// ReadAssetFile retrieves a file from AssetFS.
// Panics if file does not exist.
// TODO: Proper error handling
func ReadAssetFile(path string) []byte {
	fullPath := "assets/" + path
	data, err := AssetFS.ReadFile(fullPath)
	if err != nil {
		log.Fatalf("Could not read embedded asset %s: %s\n", path, err)
	}
	return data
}
