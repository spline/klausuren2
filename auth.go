package main

import (
	"log"
	"net"
	"net/http"
)

// IsAuthorized is a middleware that permits processing the request if it comes from whitelisted CIDR ranges.
// Otherwise, the handler for unauthorized requests is executed instead.
func IsAuthorized(authorizedHandler http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		addrStr, _, err := net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			// TODO: Instead of crashing whole app work with contexts
			log.Fatalf("Failed to split remote IP address from port: %s\n", err)
		}
		addr := net.ParseIP(addrStr)
		if addr == nil {
			// TODO: Instead of crashing whole app work with contexts
			log.Fatalf("Failed to parse remote IP address\n")
		}

		// Is part of an allowed CIDR range?
		isAuth := false
		for _, cidr := range GlobalConfig.SecurityConfig.AllowedCidrs {
			if cidr.Contains(addr) {
				isAuth = true
				break
			}
		}

		if isAuth {
			authorizedHandler.ServeHTTP(rw, r)
		} else {
			unauthorizedHandler(rw, r)
		}
	})
}

func unauthorizedHandler(w http.ResponseWriter, r *http.Request) {
	// TODO: Better logging
	log.Printf("Unauthorized attempt to request %s\n", r.URL)
	html, err := AssetFS.ReadFile("cidr_unauthorized.html")
	if err != nil {
		log.Fatalf("Could not read embedded HTML file: %s\n", err)
	}
	w.WriteHeader(http.StatusUnauthorized)
	w.Write(html)
}
