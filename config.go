package main

import (
	"io/ioutil"
	"log"
	"net"
	"os"
	"path/filepath"
	"unicode/utf8"

	"github.com/BurntSushi/toml"
	"github.com/gabriel-vasile/mimetype"
)

type Config struct {
	StateDirectory string         `toml:"state_directory"`
	SecurityConfig SecurityConfig `toml:"security"`
	UploadConfig   UploadConfig   `toml:"upload"`
	StudiesConfig  StudiesConfig  `toml:"studies"`
}

type SecurityConfig struct {
	AllowedCidrsRaw []string `toml:"allowed_cidrs"`
	AllowedCidrs    []net.IPNet
}

type UploadConfig struct {
	AllowedMimetypesRaw []string `toml:"allowed_mimetypes"`
	AllowedMimetypes    []mimetype.MIME
	MaxSizeBytes        uint64 `toml:"max_size_bytes"`
	FormStartYear       uint64 `toml:"form_start_year"`
}

type StudiesConfig struct {
	Informatik     []string `toml:"informatik"`
	Bioninformatik []string `toml:"bioinformatik"`
	Mathematik     []string `toml:"mathematik"`
}

// GlobalConfig holds the application's global configuration.
// Should not be accessed by write outside of config.go.
var GlobalConfig Config

// isConfigComplete checks if all fields in the config file have been set.
// The second argument is the name of the missing key, if any.
// Needed because the TOML library can't do this natively.
func isConfigComplete() (bool, string) {
	// TODO: Implement the rest
	gc := GlobalConfig
	if gc.StateDirectory == "" {
		return false, "state_directory"
	}

	sec := gc.SecurityConfig
	if len(sec.AllowedCidrs) == 0 {
		return false, "security.allowed_cidrs"
	}

	uc := gc.UploadConfig
	if len(uc.AllowedMimetypes) == 0 {
		return false, "upload.allowed_mimetypes"
	}
	if uc.MaxSizeBytes == 0 {
		return false, "upload.max_size_bytes"
	}
	if uc.FormStartYear == 0 {
		return false, "upload.form_start_year"
	}

	sc := GlobalConfig.StudiesConfig
	if len(sc.Bioninformatik) == 0 {
		return false, "studies.bioinformatik"
	}
	if len(sc.Informatik) == 0 {
		return false, "studies.informatik"
	}
	if len(sc.Mathematik) == 0 {
		return false, "studies.mathematik"
	}

	return true, ""
}

func parseConfig(path string) {
	// Read config from disk
	confBytes, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalf("Failed to load configuration from disk: %s\n", err)
	}
	if !utf8.Valid(confBytes) {
		log.Fatalf("Failed to parse configuration: Config file is not valid UTF-8\n")
	}
	confStr := string(confBytes)

	// Parse it as TOML
	md, err := toml.Decode(confStr, &GlobalConfig)
	if err != nil {
		log.Fatalf("Failed to parse configuration: %s\n", err)
	}
	if len(md.Undecoded()) > 0 {
		log.Fatalf("Failed to parse config file: Unknown keys %s\n", md.Undecoded())
	}

	// Parse strings into more expressive objects
	parseConfigAllowedCIDRs()
	parseConfigAllowedMIMEtypes()

	haveMissing, missing := isConfigComplete()
	if !haveMissing {
		log.Fatalf("Failed to parse config file: Missing or invalidly-valued key %s\n", missing)
	}

	ensureStateDirectoryExists()

	log.Println("Configuration parsed successfully")
}

func parseConfigAllowedCIDRs() {
	for _, str := range GlobalConfig.SecurityConfig.AllowedCidrsRaw {
		_, cidr, err := net.ParseCIDR(str)
		if err != nil {
			log.Fatalf("Failed to parse configuration: %s\n", err)
		}
		GlobalConfig.SecurityConfig.AllowedCidrs = append(GlobalConfig.SecurityConfig.AllowedCidrs, *cidr)
	}
}

func parseConfigAllowedMIMEtypes() {
	for _, str := range GlobalConfig.UploadConfig.AllowedMimetypesRaw {
		mime := mimetype.Lookup(str)
		GlobalConfig.UploadConfig.AllowedMimetypes = append(GlobalConfig.UploadConfig.AllowedMimetypes, *mime)
	}
}

func ensureStateDirectoryExists() {
	p, err := filepath.Abs(GlobalConfig.StateDirectory)
	if err != nil {
		log.Fatalf("Failed to convert state directory path to absolute: %s\n", err)
	}

	info, err := os.Stat(p)
	if err != nil {
		if os.IsNotExist(err) {
			// Try to create it
			log.Printf("Creating state directory at %s\n", p)
			err = os.MkdirAll(p, 0750)
			if err != nil {
				log.Fatalf("Failed to create state directory: %s\n", err)
			}
		} else {
			log.Fatalf("Failed to check whether state directory exists: %s\n", err)
		}
	}

	info, err = os.Stat(p)
	if !info.IsDir() {
		log.Fatalf("State directory %s is unsuitable because it's not a directory", p)
	}
}
