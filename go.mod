module spline.de/klausuren2

go 1.16

require (
	github.com/BurntSushi/toml v0.4.1
	github.com/gabriel-vasile/mimetype v1.4.0
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/google/uuid v1.3.0
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/mod v0.5.0 // indirect
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
	golang.org/x/sys v0.0.0-20211210111614-af8b64212486 // indirect
	golang.org/x/tools v0.1.5 // indirect
	modernc.org/sqlite v1.14.3
)
