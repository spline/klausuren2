package main

import (
	"database/sql"
	"log"
	"path/filepath"

	"github.com/google/uuid"
	_ "modernc.org/sqlite"
)

func InitDb() *sql.DB {
	dbPath := filepath.Join(GlobalConfig.StateDirectory, "db.sqlite")
	db, err := sql.Open("sqlite", dbPath)
	if err != nil {
		log.Fatalf("Failed to open sqlite database: %s\n", err)
	}

	err = createDbSchema(db)
	if err != nil {
		log.Fatalf("Failed to initialize database schema: %s\n", err)
	}

	return db
}

func createDbSchema(db *sql.DB) error {
	cmd := `
		CREATE TABLE IF NOT EXISTS documents (
		document_id CHAR(36) UNIQUE PRIMARY KEY NOT NULL,
		degree CHAR(64) NOT NULL,
		module CHAR(64) NOT NULL,
		year INT NOT NULL
		);
	`
	_, err := db.Exec(cmd)
	return err
}

// CreateDbDocument creates a new document in the given database.
// Returns the ID of the document, or an error on failure.
func CreateDbDocument(db *sql.DB, degree string, module string, year uint64) (string, error) {
	stmt, err := db.Prepare("INSERT INTO documents VALUES (?, ?, ?, ?);")
	if err != nil {
		return "", err
	}

	id := uuid.NewString()
	_, err = stmt.Exec(id, degree, module, year)
	if err != nil {
		return "", err
	}

	return id, nil
}
