package main

import (
	"flag"
	"log"
	"net/http"
)

func main() {
	configPath := parseCLI()
	parseConfig(configPath)

	db := InitDb()
	defer db.Close()

	http.HandleFunc("/", notFoundHandler)
	http.HandleFunc("/index.html", indexHandler)

	// TODO: Make port and listen address configurable
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func notFoundHandler(w http.ResponseWriter, r *http.Request) {
	// TODO: Better logs
	log.Printf("Sending 404 for access on %s\n", r.URL)
	html := ReadAssetFile("404.html")
	w.WriteHeader(http.StatusNotFound)
	_, err := w.Write(html)
	if err != nil {
		log.Fatalf("Failed to write index.html: %s\n", err)
	}
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	handler := IsAuthorized(http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		html := ReadAssetFile("index.html")
		w.Write(html)
	}))
	handler.ServeHTTP(w, r)
}

func parseCLI() string {
	configPath := flag.String("config", "klausuren2.toml", "Path to configuration file.")

	flag.Parse()

	return *configPath
}
